/*
  Author: Donald Trowbridge
  Date: 11/24/2020
  Class: CST235-O500
  Statement: This work is my own.
  Last Update: 11/24/2020
  Notes:
  	-11/24/2020 : Created timer bean to display a message every 10 seconds, and create a "timeout" service which displays a message when timeout occurs.
*/

package business;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import java.util.logging.Logger;
import javax.ejb.TimerService;
import javax.annotation.Resource;
import javax.ejb.Timeout;

@Stateless
public class MyTimerService {

	private static final Logger logger = Logger.getLogger("business.MyTimerService");
	@Resource
	private TimerService timerService;
	
    /**
     * Default constructor. 
     */
    public MyTimerService() {
        // TODO Auto-generated constructor stub
    }
	
    /*
     * Display message on schedule
     */
	@SuppressWarnings("unused")
	@Schedule(second="*/10", minute="*", hour="0-23", dayOfWeek="Mon-Fri",
      dayOfMonth="*", month="*", year="*", info="MyTimer")
    private void scheduledTimeout(final Timer t) {
        logger.info("@Schedule called at: " + new java.util.Date());
    }
	
	/*
	 * Set timer
	 */
	public void setTimer(long interval) {
		timerService.createTimer(interval, "My Timer");
	}
	
	/*
	 * Displays timeout message on timeout
	 */
	@Timeout
	public void programmicTimer(Timer timer) {
		logger.info("Timeout has occured.");
	}
}