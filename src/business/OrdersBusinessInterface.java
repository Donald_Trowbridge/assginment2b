/*
  Author: Donald Trowbridge
  Date: 11/24/2020
  Class: CST235-O500
  Statement: This work is my own.
  Last Update: 11/24/2020
  Notes:
  	-11/24/2020 : Interface for Order beans
*/

package business;

import javax.ejb.Local;
import beans.Order;
import beans.Orders;
import java.util.List;

@Local
public interface OrdersBusinessInterface {
	
	public void test();
	
	public List<Order> getOrders();
	
	public void setOrders(List<Order>orders);
}
