/*
  Author: Donald Trowbridge
  Date: 11/24/2020
  Class: CST235-O500
  Statement: This work is my own.
  Last Update: 11/24/2020
  Notes:
  	-11/24/2020 : Created session bean which sets a list of orders, which is to be called on the TestResponse.jsf through the FormController.services() method. Also, displays message when test message called.
*/

package business;

import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;
import beans.Orders;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {

	private List<Order> orders;
    /**
     * Default constructor. 
     */
    public OrdersBusinessService() {
    	
    	/*
    	 * Create ArrayList and populate it with dummy data
    	 */
    	orders = new ArrayList<Order>();
		orders.add(new Order("9026385927", "PS4", 399.99F, 1));
		orders.add(new Order("9026385927", "PS4 Dual Shock Controller", 69.99F, 2));
		orders.add(new Order("9026385927", "HDMI Cable 6ft", 15.00F, 1));
		orders.add(new Order("6298638572", "Player One Coffee - Leeroy Jenkins", 15.00F, 3));
		orders.add(new Order("5026789038", "Racer Gaming Chair", 139.99F, 1));
		orders.add(new Order("2309376953", "1 TB External HardDrive", 69.00F, 2));
    }

	/*
	 * Displays message when called
	 */
    public void test() {
        System.out.println("Hello from the OrdersBusinessService.");
    }

    /*
     * Returns list of orders
     */
	@Override
	public List<Order> getOrders() {
		// TODO Auto-generated method stub
		return this.orders;
	}
	
	/*
	 * Sets list of orders
	 */
	@Override
	public void setOrders(List<Order> orders) {
		// TODO Auto-generated method stub
		this.orders = orders;
	}


}
