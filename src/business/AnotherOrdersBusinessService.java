/*
  Author: Donald Trowbridge
  Date: 11/24/2020
  Class: CST235-O500
  Statement: This work is my own.
  Last Update: 11/24/2020
  Notes:
  	-11/24/2020 : Created session bean which sets a list of orders, which is to be called on the TestResponse.jsf through the FormController.services() method. Also, displays message when test message called.
*/

package business;

import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;
import beans.Orders;

/**
 * Session Bean implementation class AnotherOrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative
public class AnotherOrdersBusinessService implements OrdersBusinessInterface {

	private List<Order> orders;
	
    /**
     * Default constructor. 
     */
    public AnotherOrdersBusinessService() {
    	/*
    	 * Create ArrayList and populate it with dummy data
    	 */
    	orders = new ArrayList<Order>();
		orders.add(new Order("9062569378", "Gala Apples - 3lb", 3.99F, 2));
		orders.add(new Order("9062569378", "Watermelon", 4.99F, 1));
		orders.add(new Order("9062569378", "Bananas - 1lb", 1.59F, 1));
		orders.add(new Order("9062569378", "Pears - 1lb", 2.69F, 1));
		orders.add(new Order("8290638263", "T-Bone Steaks - 2.5lb", 23.47F, 1));
		orders.add(new Order("7253856396", "Milk 1lb", 2.49F, 2));
		orders.add(new Order("7253856396", "Cinamon Toast Crunch", 3.50F, 2));
		orders.add(new Order("7253856396", "Cheerios", 3.00F, 1));
		orders.add(new Order("5692368592", "Red Barons Pepperoni Pizza", 3.99F, 3));
		orders.add(new Order("5692368592", "Root Beer 2L", 1.25F, 2));
		orders.add(new Order("5692368592", "Doritos Nacho Cheese", 1.25F, 2));
    }

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
        // TODO Auto-generated method stub
    	System.out.println("Hello from the AnotherOrdersBusinessService.");
    }

    /*
     * Returns list of orders
     */
	@Override
	public List<Order> getOrders() {
		// TODO Auto-generated method stub
		return this.orders;
	}

	/*
	 * Sets list of orders
	 */
	@Override
	public void setOrders(List<Order> orders) {
		// TODO Auto-generated method stub
		this.orders = orders;
	}

}
