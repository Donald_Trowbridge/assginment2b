/*
  Author: Donald Trowbridge
  Date: 11/14/2020
  Class: CST235-O500
  Statement: This work is my own.
  Last Update: 12/1/2020
  Notes:
  	-11/24/2020 : Injected OrdersBusinessInterface, add MyTimerService. Orders are now retrieved from the 
  				instantiated session bean corresponding to the OrdersBusinessInterface, and a timer is set to display a message after 10 seconds.
  	-12/1/2020  : Added methods to query database to pull all products and to add a product.
*/

package controllers;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.User;
import business.OrdersBusinessInterface;
import business.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ejb.EJB;


import javax.inject.Inject;

@ManagedBean(name="formController")
@ViewScoped

public class FormController {
	
	@Inject private OrdersBusinessInterface services;
	@EJB MyTimerService timer;
	
	public String onSubmit(User user) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getRequestMap().put("User", user);
		services.test();
		timer.setTimer(10000);
		getAllOrders();
		insertOrder();
		getAllOrders();
		return "TestResponse.xhtml";
	}
	
	public String onFlash(User user) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getFlash().put("User", user);
		return "TestResponse2.xhtml?faces-redirect=true";
	}
	
	public OrdersBusinessInterface getService(){
		return services;
	}
	
	/*
	 * Queries all orders from the orders table
	 */
	private void getAllOrders() {
		Connection conn = null;
		String query = "select * from testapp.orders";
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			System.out.println("ID\tProduct_Name\t\tPrice");
			while(resultSet.next()) {
				System.out.printf("%d\t%s\t$%.2f\n", resultSet.getInt("id"), resultSet.getString("product_name"), resultSet.getFloat("price"));
			}
			resultSet.close();
			System.out.println("Success!!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Failure!!");
		}finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	/*
	 * Inserts an product into the orders database.
	 */
	private void insertOrder() {
		Connection conn = null;
		String insertQuery = "insert into testapp.orders(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('001122334455', 'This was inserted new', 25.00, 100)";
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
			Statement statement = conn.createStatement();
			statement.executeUpdate(insertQuery);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try{
					conn.close();
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
