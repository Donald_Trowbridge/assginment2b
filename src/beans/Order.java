/*
 * Author: Donald Trowbridge
 * Date: 11/20/2020
 * Class: CST235-O500
 * Statement: This work is my own.
 * Late Update:
 * Notes:
 * 	- 11/20/2020 : Object for a order object
 */
package beans;

public class Order {

	protected String orderNumber, productName;
	protected Float price;
	protected int quantity;
	
	/*
	 * Default constructor
	 */
	public Order() {
		this.price = 0F;
		this.quantity = 0;
	}
	
	/*
	 * Construct to fill out order details
	 */
	public Order(String orderNumber, String productName, Float price, int quantity) {
		this.orderNumber = orderNumber;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
