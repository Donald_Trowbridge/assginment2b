/*
 * Author: Donald Trowbridge
 * Date: 11/13/2020
 * Class: CST235-O500
 * Statement: This work is my own.
 * Late Update: 11/20/2020
 * Notes:
 * 	- 11/20/2020 : Added annotations of data validation
 */

package beans;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.faces.bean.ManagedBean;


@ManagedBean(name="User")
@ViewScoped
public class User{

	@NotNull
	@Size(min=5, max=15)
	protected String firstName;
	protected String lastName;
	
	@NotNull
	@Size(min=5, max=15)
	public User() {
		firstName = "Donald";
		lastName = "Trowbridge";
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public String getLastName() { 
		return this.lastName;
	}
	
	public void setFirstName(String firstname) {
		firstName = firstname;
	}
	
	public void setLastName(String lastname) {
		lastName = lastname;
	}
}
